#!/bin/bash
readonly DOMAIN_PATH="$HOME/admin/glassfish/domains";
readonly ASADMIN_PATH="$HOME/admin/bin";
function list_administration_port_file(){
	#$1=domain expected
	local PORT_BASE
	local CONFIG_PATH=""${DOMAIN_PATH:?}"/"${1}"/config/";
	#local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "admin-listener\"\ port" | awk {'print $3'} | sed -E 's\"|port=\\g' )
	local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "network-listener protocol=" | grep "admin-listener" | head -1 | awk {'print $3'} | sed -E 's\"|port=\\g' )
	echo $PORT_BASE
}
list_administration_port_file "$1"
function list_administration_port_gf(){
	echo #
}
function list_http_server_port_file(){
	#$1=domain expected
	local PORT_BASE
	local CONFIG_PATH=""${DOMAIN_PATH:?}"/"${1}"/config/";
	#local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "http-listener-1\"\ port" | awk {'print $3'} | sed -E 's\"|port=\\g' )
	local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "network-listener protocol=" | grep "http-listener-1" | head -1 | awk {'print $3'} | sed -E 's\"|port=\\g' )
	echo $PORT_BASE
}
list_http_server_port_file "$1"
function list_http_server_port_gf(){
	echo #
}
