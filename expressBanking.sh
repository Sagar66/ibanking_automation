#!/bin/bash
##################
#VARIABLES
#declare -ar MUST_HAVE_APPS
					#messages
#				Only functions added. main case is not there. so script will not work properly for now

#UNDER CONSTRUCTION
echo UNDER CUNSTRUCTION

MUST_HAVE_APPS=("java" "unzip");
readonly DOMAIN_PATH="$HOME/admin/glassfish/domains";
readonly ASADMIN_PATH="$HOME/admin/bin";
readonly DEPLOYMENT_FILES_PATH="$HOME/deployment";
#echo "${#MUST_HAVE_APPS[@]}"
###############################################################
##ESS
readonly REAL_FILE=$(realpath "$0");
echo $REAL_FILE
readonly REAL_PATH=$(dirname "$(realpath $0)");
echo $REAL_PATH
[ -f "${ASADMIN_PATH}/asadmin" ] || echo "File does not exist"
[ -x "${ASADMIN_PATH}/asadmin" ] || echo "File is not an executable or does not exist"
# Applications Check
echo i am here
function base_app_check(){
	local i=0;
	local n=0;
	for program in "${MUST_HAVE_APPS[@]}";
	do
		if hash "$program" &> /dev/null
		then
			printf -- "\033[1;32m\"%s\" ----> installed.\033[0m\n" "${program}" 
			let i++
		else
			printf -- "\033[1;31m\"%s\" ----> not installed.\033[0m\n" "${program}"
			let n++
		fi
	done
if [ "$i" -eq "0" ];then
	printf "\033[1;31m\n%d of %d programs were missin.\n\033[1;33mPlease make sure those are installed and try again\033[0m\n" "$n" "${#MUST_HAVE_APPS[@]}"
elif [ "$n" -eq "0" ];then
	printf "\033[1;32m\nAll Good\033[0m\n";
else
	printf "\033[1;32m\n%d of %d programs were installed.\n\033[1;33mBut there are missin programs. which are listed above^ \033[0m\n"  "$i" "${#MUST_HAVE_APPS[@]}"
	printf "\033[1;31m\n%d of %d programs were missing.\n\033[1;33mPlease make sure those are installed and try again\033[0m\n" "$n" "${#MUST_HAVE_APPS[@]}"
fi
}
if [ ${#MUST_HAVE_APPS[@]} -gt 0 ] && [ -z "$var" ] ; then
base_app_check
fi
function is_there () {
	if [ ! -x "$(command -v "$1")" ]; then
		return 127;
	else
		return 0;
	fi
}
if [ "$1" = "glassfish" ] && [ "$2" = "install" ];then
	glassfish_install
fi
function list_all_domains(){
	cd "${DOMAIN_PATH}" && ls -I "*_*"
}
#list_all_domains
function config_check(){
	if [ -f ""${DOMAIN_PATH}"/$1/config.xml" ]; then
		echo exists
		exit 0
	else
		echo not exit
		exit 2 
	fi
}
#config_check "admin"
function valid_domains(){
	local domain
	#set -x
	local ALL_DOMAINS=($( cd "${DOMAIN_PATH}" && ls -I "*_*"));
	for domain in "${ALL_DOMAINS[@]}";
	do
		if [ -f """${DOMAIN_PATH}"/"$domain"/config/domain.xml"" ] && [ -d ""${DOMAIN_PATH}"/"$domain"/logs" ]; then
			printf "%s\n" "$domain";
		fi
	done
	#set +x
}
#valid_domains
function list_domains_all(){
	local ASADMIN="$ASADMIN_PATH/asadmin";
	$ASADMIN list-domains | grep "running" | awk '{print $1}';
}
#list_domains_all
function list_domains_running(){
	local ASADMIN="$ASADMIN_PATH/asadmin";
        $ASADMIN list-domains | grep -v '^.*_.*$' | grep "running" | grep -v "not running" | awk '{print $1}';
}
#echo runnings
#list_domains_running
function list_domains_not_running(){
	local ASADMIN="$ASADMIN_PATH/asadmin";
	$ASADMIN list-domains | grep -v '^.*_.*$' | grep "not running" | awk '{print $1}';
}
#echo not running
#list_domains_not_running
function list_domains_backups(){
	local ASADMIN="$ASADMIN_PATH/asadmin";
	$ASADMIN list-domains | grep '^.*_.*$' | awk '{print $1}';
}
#echo back up domains
#list_domains_backups
function list_all_domain_ports(){
	#local valid_domains=(list_domains_running);
	local domain
	valid_domains | while read domain
do
	local CONFIG_PATH=""${DOMAIN_PATH:?}"/"${domain}"/config/";
	local CONFIG_PATH=""${DOMAIN_PATH:?}"/"${domain}"/config/";
	local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "network-listener protocol=" | head -n 2 | awk {'print $3'} | sed -e '0,/port=/ s/port=/FrontEnd Port=/'| sed -e '0,/port=/ s/port=/BackEnd Port=/' )
	if [ -n "${PORT_BASE}" ];then
		echo "********************************************";
                echo "${domain}";
                echo -e "\t"${BOLD}""${GREEN}""${PORT_BASE}""${NONE}""
                echo -e "********************************************\n";
	else
		echo "********************************************";
                echo -e "\033[1m${domain}\033[0m";
                echo -e "\t${RED}Config file dosent exist or grep incorrect${NONE}";
                echo -e "********************************************\n";
        fi
done
}
function list_all_domain_ports_by_glassfish(){
echo #
}
#echo list_all_domain_ports
#list_all_domain_ports
function list_running_domain_by_pid(){
	#local valid_domains=(list_domains_running);
	local domain
	valid_domains | while read domain
	do
		local CONFIG_PATH=""${DOMAIN_PATH:?}"/"${domain}"/config/";
		local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep -m 1 -h "network-listener protocol=" | awk {'print $3'} | sed -e 's/port=\"//' -e 's/\"//' )
		echo $PORT_BASE
		if [ -n "${PORT_BASE}" ];then
			port=$( ss -tlp sport = :"${PORT_BASE}" | awk {'print $6'} | tail -1 | awk -F, {'print $2'} | sed s/pid=// )
			echo "********************************************";
			echo -e ""${BOLD}""${GREEN}""${domain}" is running on $port "${NONE}"";
			echo -e "\t"${BOLD}""${GREEN}""${PORT_BASE}""${NONE}""
			echo -e "********************************************\n";
		else
			echo "********************************************";
			echo -e "\033[1m${domain}\033[0m";
			echo -e "\t${RED}Config file dosent exist${NONE}";
			echo -e "********************************************\n";
		fi
	done
}
#echo list_running_domain_by_pid
#list_running_domain_by_pid
#set -xv
if [ "$1" == "glassfish" ] && [ "$2" == "install" ];then
#	glassfish_install "${3}"
function glassfish_install(){
	if [[ -z "$1" ]];then
		printf "\033[1;92mPease specify glassfish to be installed like below:\n
		\033[1;93m%s <glassfish_name>.zip
		\033[1;94mNote: Download latest glassfish from \'\033[4mhttps://javaee.github.io/glassfish/download\033[0m\\033[1;94m'\033[0m\n\n" "$0";
	#if ! [[ $x =~ $y ]] or if [[ ! $x =~ $y ]]
	elif [[ ! "$3" =~ ^.*\.zip ]];then
		printf "\033[1;31m*Not valid file format or file dosen\'t exist\n\tMake sure file is of .zip format\033[0m\n"
	else
		read -p 'Please Enter Glassfish Name: ' NAME
		#unzip $1
	fi	
}
glassfish_install "${3}"
fi
if [ "$1" == "create" ] && [ "$2" == "domain" ];then
function create_domain_glassfish(){
	if [[ -z "$1" ]];then
		printf "\033[1;92mPease specify glassfish domain to be created like below:\n
		\033[1;93mSyntax: %s create domain <domain_name>
		\033[1;93mLike: %s create domain admin\033[0m\n\n" "$0";
	else
		printf "\n\033[1;33mPlease enter port to be used\nIf port base=8000, Server Port will be 8080 & Management port will be 8048\nPlease enter free port below accordinly \n"
		local PORTBASE
		read -p "Please Enter portbase to be used: " PORTBASE
		while [[ ! "${PORTBASE}" =~ ^[+-]?[0-9]...+ ]];do
			printf "\033[0m\033[1;91mEntered Portbase is invalid. Try again\033[0m\n"
			read -p "Please Enter portbase to be used: " PORTBASE
		done
		printf "\033[1;92m\nCreating domain. may take few minuts: \033[0m\033[1;34m\n";
		"${ASADMIN_PATH}"/asadmin create-domain --user admin --portbase "${PORTBASE}" "$1"
		if [ "$?" -eq "0" ];then
			echo susex
		else
			printf "\t\t\033[0m\033[1;5;31mError code=%d\n" "$?"
			printf "Domain create failed.!!! Please review logs and proceed again.\033[0m\n"
			exit 2
		fi
	fi
}
create_domain_glassfish "$3"
fi
function is_domain_running(){
	local domain read
	list_domains_running | while read domain
do
	if [ "$domain" == "$1" ];then
		printf "%s" "$domain";
	fi
done
}
function is_deployed_in_any_domain(){
	local domain read
	local ALL_DOMAINS=$(valid_domains);
	local EAR_NAME=${1::-4}
	local FILE="${DEPLOYMENT_FILES_PATH}"/"$1";
	valid_domains | while read domain
	do
		if [ -f "${DOMAIN_PATH}"/"${domain}"/applications/"${EAR_NAME}" ];then
			printf "%s" "${domain}";
		fi
	done
}
function is_deployed_in_domain(){
	local domain read
	local EAR_NAME=${2::-4}
	#local FILE="${DEPLOYMENT_FILES_PATH}"/"$1";
	
	if [ -d "${DOMAIN_PATH}"/"$1"/applications/"${EAR_NAME}" ];then
		return 0;
	else
		return 1;
	fi

}
function stop_domain(){
	"${ASADMIN_PATH}"/asadmin stop-domain "$1"
	[[ "$?" -eq "0" ]] && return 0 || return 1;
}
function start_domain(){
	"${ASADMIN_PATH}"/asadmin start-domain "$1"
	[[ "$?" -eq "0" ]] && return 0 || return 1;
}
function restart_domain(){
	"${ASADMIN_PATH}"/asadmin restart-domain "$1"
	[[ "$?" -eq "0" ]] && return 0 || return 1;
}
function action_domain(){
	"${ASADMIN_PATH}"/asadmin "$1"-domain "$2"
	[[ "$?" -eq "0" ]] && return 0 || return 1;
}
function secure_admin_port(){
	echo #
}
function list_administration_port_file(){
	#$1=domain expected
	local PORT_BASE
	local CONFIG_PATH=""${DOMAIN_PATH:?}"/"${1}"/config/";
	#local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "admin-listener\"\ port" | awk {'print $3'} | sed -E 's\"|port=\\g' )
	local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "network-listener protocol=" | grep "admin-listener" | head -1 | awk {'print $3'} | sed -E 's\"|port=\\g' )
	echo $PORT_BASE
}
function list_http_server_port_file(){
	#$1=domain expected
	local PORT_BASE
	local CONFIG_PATH=""${DOMAIN_PATH:?}"/"${1}"/config/";
	#local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "http-listener-1\"\ port" | awk {'print $3'} | sed -E 's\"|port=\\g' )
	local PORT_BASE=$( cd "${CONFIG_PATH}" 2>/dev/null && cat domain.xml 2>/dev/null | grep "network-listener protocol=" | grep "http-listener-1" | head -1 | awk {'print $3'} | sed -E 's\"|port=\\g' )
	echo $PORT_BASE
}
function list_administration_port_gf(){
	#$1=domain expected
	echo #
}
function list_http_server_port_gf(){
	#$1=domain expected
	echo #
	#get server-config.network-config.network-listeners.network-listener.http-listener-1.port | head -1 | awk -F= '{print $2}'
}
function list_administration_port_gf(){
	echo #
}
if [ "$1" == "deploy" ];then
function deploy_domain_ear_menu(){
	#set -vx
	#$1=ear file name expected
	#Verify ear exists
	local domain
	local ALL_DOMAINS=$(valid_domains);
	local RUNNING_DOMAINS=$(list_domains_running);
	local FILE="${DEPLOYMENT_FILES_PATH}"/"$1";
	if [ -z "$1" ]; then
		printf "please specify file name to be deployed\n";
	elif [ ! -f "${FILE}" ]; then
		local EAR_NAME=${1::-4}
		printf "\033[1;31mDeployment file you mentioned \"%s\" dosent exist in deployment directory: %s\033[0m\n" "$1" "${DEPLOYMENT_FILES_PATH}";
	else
		local EAR_NAME=${1::-4}
		echo hgggghggghhg $EAR_NAME
		printf "Deployment file exists.\n";
		#printf "%s)\n" "${ALL_DOMAINS}";
		printf "Select domain to be deployed:\n";
		#IFS is feild seprator
		local domain item deploymentChoice
		local oldIFS=$IFS
		local IFS=$'\n'
		local choices=( ${ALL_DOMAINS} exit )
		IFS=$oldIFS
		local PS3="In which domain you want to deploy?(\$number): "
		select domain in "${choices[@]}"; do
			for item in "${choices[@]}"; do
 				if [[ $item == $domain ]]; then
					break 2
				fi
			 done
		done
		printf "Your Choice is \"%s\"\nChecking if \"%s\" is up or not...\n" "$domain" "$domain";
		if [ "$(is_domain_running "$domain")" == "${domain}" ];then
			printf "Domain is running\n";
			printf "Checking if already deployed:-\n"
			if is_deployed_in_domain "$domain" "$1";then
				printf "\"%s\" ear is already deployed in \"%s\" on port \"%s\"\n" "$1" "$domain" $(list_administration_port_file "$domain");
				read -e -p "Do You want to re-deploy it?(y/n) : " -i "N" redeploymentChoice
				deploymentChoice="${redeploymentChoice:-n}"
				case "${redeploymentChoice}" in
					y|Y|yes|Yes|0) echo your choice is y or Y
						printf "Undeploying \"%s\" on \"%s\" with port \"%d\"\n" "$1" "$domain" $(list_administration_port_file "$domain")
						echo ear name is == $EAR_NAME
						"${ASADMIN_PATH}"/asadmin undeploy --port $(list_administration_port_file "$domain") "${EAR_NAME}"
						if [ "$?" -eq "0" ];then
							printf "Undeployment successful:\nDeploying file again"
							printf "Deploying \"%s\" on \"%s\" with port \"%d\"\n" "$1" "$domain" $(list_administration_port_file "$domain")
							"${ASADMIN_PATH}"/asadmin deploy --port $(list_administration_port_file "$domain") "${DEPLOYMENT_FILES_PATH}"/"$1"
						else
							printf "Error during undeployment. Please try again"
							exit 1;
						fi
						;;
					*) echo Exiting script gracefully..
						exit 1;
						;;
				esac
			else
				printf "Ear is not deployed there\n";
				printf "\tAdministration Port: "
				list_administration_port_file "$domain"
				printf "\tHTTP Server Port: " 
				list_http_server_port_file "$domain"
				read -e -p "Do You wan't to deploy now(Enter \"y\" to continue or any other key to exit): " -i "Y" deploymentChoice
				deploymentChoice=${deploymentChoice:-y}
				case "${deploymentChoice}" in
					y|Y|yes|Yes|0) echo your choice is y or Y
						printf "Deploying \"%s\" on \"%s\" with port \"%d\"\n" "$1" "$domain" $(list_administration_port_file "$domain")
						"${ASADMIN_PATH}"/asadmin deploy --port $(list_administration_port_file "$domain") "${DEPLOYMENT_FILES_PATH}"/"$1"
						;;
					*) echo Exiting script gracefully..
						exit 1;
						;;
				esac
			fi
		else
			printf "Domain is not running\n";
			read -e -p "Do You wan't to start domain now(Enter \"y\" to continue or any other key to exit): " -i "Y" domainStartChoice
			domainStartChoice=${domainStartChoice:-y}
			case "${domainStartChoice}" in
				y|Y|yes|Yes|0) echo your choice is y or Y
					printf "Starting domain...\n";
					start_domain "$domain"
					[[ "$?" -eq "0" ]] && printf "Domain started\n" || { printf "Something went wrong!!!";exit 1; }
					printf "Checking if already deployed:-\n"
					if is_deployed_in_domain "$domain" "$1";then
						printf "\"%s\" ear is already deployed in \"%s\" on port \"%s\"\n" "$1" "$domain" $(list_administration_port_file "$domain");
						read -e -p "Do You want to re-deploy it?(y/n) : " -i "N" redeploymentChoice
						deploymentChoice=${redeploymentChoice:-n}
						case "${redeploymentChoice}" in
							y|Y|yes|Yes|0) echo your choice is y or Y
							printf "Undeploying \"%s\" on \"%s\" with port \"%d\"\n" "$1" "$domain" $(list_administration_port_file "$domain")
							"${ASADMIN_PATH}"/asadmin undeploy --port $(list_administration_port_file "$domain") "${EAR_NAME}"
								if [ "$?" -eq "0" ];then
									printf "Undeployment successful:\nDeploying file again"
									printf "Deploying \"%s\" on \"%s\" with port \"%d\"\n" "$1" "$domain" $(list_administration_port_file "$domain")
									"${ASADMIN_PATH}"/asadmin deploy --port $(list_administration_port_file "$domain") "${DEPLOYMENT_FILES_PATH}"/"$1"
								else
									printf "Error during undeployment. Please try again"
									exit 1;
								fi
								;;
							*) echo Exiting script gracefully..
								exit 1;
								;;
						esac
					else
						printf "Ear is not deployed there\n";
						printf "\tAdministration Port: "
						list_administration_port_file "$domain"
						printf "\tHTTP Server Port: " 
						list_http_server_port_file "$domain"
						read -e -p "Do You wan't to deploy now(Enter \"y\" to continue or any other key to exit): " -i "Y" deploymentChoice
						deploymentChoice=${deploymentChoice:-y}
						case "${deploymentChoice}" in
							y|Y|yes|Yes|0) echo your choice is y or Y
							printf "Deploying \"%s\" on \"%s\" with port \"%d\"\n" "$1" "$domain" $(list_administration_port_file "$domain")
							"${ASADMIN_PATH}"/asadmin deploy --port $(list_administration_port_file "$domain") "${DEPLOYMENT_FILES_PATH}"/"$1"
							;;
						*) echo Exiting script gracefully..
							exit 1;
							;;
						esac
					fi

							;;
						*) echo Exiting script gracefully..
						exit 1;
							;;
			esac

		fi
	fi
}
deploy_domain_ear_menu "$2"
fi
function deploy_domain_ear(){
	#set -vx
	#Verify ear exists
	local domain
	local ALL_DOMAINS=$(valid_domains);
	local RUNNING_DOMAINS=$(list_domains_running);
	local EAR_NAME=${1::-4}
	local FILE="${DEPLOYMENT_FILES_PATH}"/"$1";
	if [ ! -f "${FILE}" ]; then
		printf "\033[1;31mDeployment file you mentioned \"%s\" dosent exist in deployment directory: %s\033[0m\n" "$1" "${DEPLOYMENT_FILES_PATH}";
	else
		printf "Deployment file exists\nListing all avaliable domains:\n\t("
		printf "%s)\n" "${ALL_DOMAINS}";
		printf "List running domains:\n\t("
		printf "%s)\n" "${RUNNING_DOMAINS} ";
		printf "Checking if file already deployed in running domains:\nChecking....\n"
		if [ -n "is_deployed_in_any_domain "$1"" ];then
			printf "Domain is not deployed anywhere:-\n";
		fi
	fi
}
