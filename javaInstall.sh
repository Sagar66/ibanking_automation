#!/bin/bash
#================================================================
# HEADER
#================================================================
#% SYNOPSIS
#+    ${SCRIPT_NAME}
#%
#% DESCRIPTION
#%    This is a script used to just install java development kit..
#%
#% OPTIONS
#% Just execute for now.
#%	!!!-OPEN FOR ANY ADVISE-!!!
#% 
#% EXAMPLES
#%  ${SCRIPT_NAME} $1
#%
#================================================================
#- IMPLEMENTATION
#-    version         ${SCRIPT_NAME} 0.0.0.1 (alpha)
#-    author          Sagar Khatiwada(sagar.khatiwada66@gmail.com)
#-    copyright       Enjoy
#-    license         N/A
#-    script_id/      0000
#-    PushCount		
#================================================================
#  HISTORY
#     2020/04/06 : sks63 : Script creation
# 
#================================================================
#  DEBUG OPTION
#    set -n  # Uncomment to check your syntax, without execution.
#    set -x  # Uncomment to debug this shell script
#    set -v  # Uncomment to debug on verbose mode
#================================================================
#  BUGS
#    -- under development
#
#================================================================
# END_OF_HEADER
#================================================================
# BEGAIN
#================================================================
# Exit on error/un used variable check/pipe fail check
set -o errexit 
set -o nounset
set -o pipefail
#================================================================
# VARIABLES


#================================================================
##BEGAIN
#Check if java exists
if [ -x "$(command -v java)" ];then
	printf -- '\033[1;32m\nJava is already here with following version: \033[0m\n';
	JAVA="$(java -version | head -1)"
	exit 0
else
	printf -- '\033[1;33Java is not installed\033[0m\n';
fi
read -p "Proceed installation? (y/n): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo
	echo "Installing Sys";
	#tar xfva "$1" --one-top-level="$HOME/.java" --strip-components 1
	echo -e "JAVA=\$HOME/.java/bin\nPATH=\$JAVA:\$PATH" >> $HOME/.bash_profile;
	source $HOME/.bash_profile
else
	exit 1
fi
