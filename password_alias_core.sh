#!/bin/bash
#$1=domain
#$2=port
function recreate_password_alias(){
	[[ -z "$1" ]] && { printf "Invalid argument applied\n";exit 1;}
	local PASSWORD
	printf "Removing existing alias";
	~/admin/bin/asadmin delete-password-alias "${1}"_login
	if [ "$?" -eq "0" ];then
		printf "%d_login alias removed\n";
		printf "Recreating alias again\n";
		printf "setting password file\n"
		read -p "Enter domain management password: " PASSWORD
		printf "AS_ADMIN_ALIASPASSWORD=%s" "$PASSWORD" > "$1"_password_alias.tmp
		~/admin/bin/asadmin --passwordfile "$1"_password_alias.tmp create-password-alias "${1}"_login
		if [ "$?" -eq "0" ];then
			printf "\"$s\" password is set with alias name: \"${1}_login\"\nPlease use it as below:\n${ALIAS=${1}_login}"
		else
			printf "Error executing command\nPlease review log and if alias already exists and want create alias again?"
		fi

	else
		printf "Error executing command\nPlease review log and try again";
		exit 1;
	fi
}	
function create_password_alias(){
	[[ -z "$1" ]] && { printf "Invalid argument applied\n";exit 1;}
	local PASSWORD
	printf "setting password file\n"
	read -p "Enter domain management password: " PASSWORD
	printf "AS_ADMIN_ALIASPASSWORD=%s" "$PASSWORD" > "$1"_password_alias.tmp
	~/admin/bin/asadmin --passwordfile "$1"_password_alias.tmp create-password-alias "${1}"_login
	if [ "$?" -eq "0" ];then
		printf "\"$s\" password is set with alias name: \"${1}_login\"\nPlease use it as below:\n${ALIAS=${1}_login}"
	else
		printf "Error executing command\nPlease review log and if alias already exists and want create alias again?"
	fi
}
create_password_alias "$1"
function create_password_alias_core(){
	#$1=domain $2=port #3=password to set expected!!
	printf "AS_ADMIN_ALIASPASSWORD=%s" "$PASSWORD" > "$3"_password_alias.tmp
	[[ "$?" -gt "0" ]] && return 132;
	$ASADMIN_FILE --passwordfile "$1"_password_alias.tmp create-password-alias "${1}"_login --port "$3"
	if [ "$?" -eq "0" ];then
		return 0;
	else
		return 1;
	fi
}
function delete_password_alias_core(){
	#$1=domain $2=port #3=password to set expected!!
	$ASADMIN_FILE delete-password-alias "${1}"_login --port "$3"
	if [ "$?" -eq "0" ];then
		return 0;
	else
		return 1;
	fi
}
