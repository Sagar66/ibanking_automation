# 1). MySql backup and rotate script.
       --> https://gitlab.com/Sagar66/mysql_rotate

# 2). Expressbank log backup and rotate.
	--> https://gitlab.com/Sagar66/log_rotate_expressbank

# 3). Banksmart log backup and rotate.
	--> https://gitlab.com/Sagar66/log_rotate_banksmart

# 4). Certificate Expiry check.
	--> https://gitlab.com/Sagar66/certificate_expiry

# 5). Domain Expiry Check.
	--> https://gitlab.com/Sagar66/domain_check

# 6). Mail related tips.
	--> https://gitlab.com/Sagar66/mail_send

# 7). Cheat Sheet
	--> https://gitlab.com/Sagar66/shortcut
	--> curl cheat.sh/<anything>
	
# 8) IPS API( API and text manipulation knowledge)
	--> https://gitlab.com/Sagar66/ips-api

# 9) Password Generation Hashing (Bcrypt)
	--> https://gitlab.com/Sagar66/password_hash
